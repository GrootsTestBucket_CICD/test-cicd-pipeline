import unittest
import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
import flaskApp
import json

class FlaskTest(unittest.TestCase):

    def setUp(self):
        flaskApp.app.testing = True
        self.app = flaskApp.app.test_client()

    def test_home(self):
        output = self.app.get('/')
        output = json.loads(output.data.decode("utf-8").replace("\n",""))
        self.assertEqual(output["type"],"success")

if __name__ == '__main__':
    unittest.main()
