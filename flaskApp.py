import flask

from datetime import datetime

from flask import request, jsonify


"""
DOCUMENTATION
=============

employees-api

installation
============
python -m pip install flask


ep.1: "/"
=======
description
===========
- Host endpoint home


ep.2: "/api/employees/_all/"
===========================
description
===========
- Get all employees in the database


ep.3: "/api/employee/<employee_id>/"
====================================
description
===========
- Get the employee with `employee_id`

"""

app = flask.Flask(__name__)
app.config["DEBUG"] = True

DATABASE = [
	{
		"uid": "101",
		"name": "Gaurav K",
		"designation": "DevOps Lead",
	},
	{
		"uid": "102",
		"name": "Atharva K",
		"designation": "Senior Data Scientist",
	},
	{
		"uid": "103",
		"name": "Ratan Tata",
		"designation": "President Emeritus",
	}
]


@app.route("/", methods=["GET"])
def home():
	return jsonify({
		"timestamp": datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f"),
		"status_code": 200,
		"type": "success",
		"message": "Welcome to BitBucket CI/CD Pipeline Endpoint home!",
	})


@app.route("/api/", methods=["GET"])
def api_ep():
	return jsonify({
		"timestamp": datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f"),
		"status_code": 404,
		"type": "error",
		"message": "The endpoint '/api/' was not found!",
	})


@app.route("/api/employees/", methods=["GET"])
def api_ep_employees():
	return jsonify({
		"timestamp": datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f"),
		"status_code": 404,
		"type": "error",
		"message": "The endpoint '/api/employees' was not found!",
	})


@app.route("/api/employee/", methods=["GET"])
def api_ep_employee():
	return jsonify({
		"timestamp": datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f"),
		"status_code": 404,
		"type": "error",
		"message": "The endpoint '/api/employee' was not found!",
	})


@app.route("/api/employees/_all/")
def get_employees_all():
	return jsonify(DATABASE)


@app.route("/api/employee/<eid>/")
def get_employee_by_id(eid):
	eid_record = [e for e in DATABASE if str(e["uid"]) == str(eid)]
	
	if len(eid_record) == 0:
		output = {
			"timestamp": datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f"),
			"status_code": 404,
			"type": "error",
			"message": f"The employee '{eid}' was not found!",
		}
	else:
		output = {
			"timestamp": datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f"),
			"status_code": 200,
			"type": "success",
			"message": eid_record[0],
		}

	return jsonify(output)


# Run the app server
if __name__ == '__main__':
	app.run()
